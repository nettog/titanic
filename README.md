# Titanic project

This project takes csvs relating to the titanic disaster from Kaggle, then:

* Does some data wrangling
* Produces some high level descriptives
* Make a model
* Validate the model
* Make an application to make predictions off

Output is a markdown, with some embedded shiny applications.
